import json
from multiprocessing import Pool, freeze_support

import nltk
from nltk.sentiment import SentimentIntensityAnalyzer
from nltk.stem import PorterStemmer, WordNetLemmatizer
from nltk.tokenize import word_tokenize

file_path = "./all_beauty.json"


def process_line(line):
    # Function to process each line and extract review text
    line = line.strip()
    if not line:
        return None

    json_object = json.loads(line)
    if "reviewText" in json_object:
        return json_object["reviewText"]


def process_review(review):
    ps = PorterStemmer()
    stemmed_words = [ps.stem(word[0]) for word in review]

    lemmatizer = WordNetLemmatizer()
    lemmatized_words = [lemmatizer.lemmatize(word[0]) for word in review]

    pos_tags = nltk.pos_tag([word[0] for word in review])
    named_entities = nltk.ne_chunk(pos_tags, binary=True)

    reviews_with_named_entities = []
    for word in named_entities:
        if hasattr(word, 'label'):
            reviews_with_named_entities.append(word)

    return {
        'stemmed': stemmed_words,
        'lemmatized': lemmatized_words,
        'ner': named_entities,
        'nes_list': reviews_with_named_entities,
    }


# Open the file and read its contents
with open(file_path, "r") as file:
    lines = file.readlines()

# Use multiprocessing.Pool to parallelize the processing of lines
if __name__ == '__main__':
    freeze_support()

    with Pool() as pool:
        # Process the lines in parallel and filter out None values
        reviews = pool.map(process_line, lines)
        reviews = [review for ix, review in enumerate(reviews) if review is not None and ix % 1000 == 0]

        # Tokenize and POS tag the reviews in parallel
        tokenized_reviews = pool.map(nltk.pos_tag, [word_tokenize(review) for review in reviews])

        ner_result = pool.map(process_review, tokenized_reviews)

        sia = SentimentIntensityAnalyzer()

        # Analyze the sentiment of each review in parallel
        sentiments = pool.map(sia.polarity_scores, reviews)

        # We'll just use the "compound" score for simplicity
        compound_scores = [sentiment["compound"] for sentiment in sentiments]

        final_results = []
        for i in range(len(reviews)):
            # print(ner_result[i])
            final_results.append({
                'review': reviews[i],
                'sentiment': sentiments[i],
                'ner_list': ner_result[i],
                'tokenized_review': tokenized_reviews[i],
            })

        with open('final_results.json', 'w+') as f:
            json.dump(final_results, f)

        with open('tokenized_reviews.json', 'w+') as f:
            json.dump(tokenized_reviews, f)

        with open('sentiments.json', 'w+') as f:
            json.dump(sentiments, f)

        with open('compound_scores.json', 'w+') as f:
            json.dump(compound_scores, f)
