# DZW semestrální práce

Kvůli velikosti dat jsou JSON soubory zkomprimované pomocí GZip komprese.

Na systémech s Linuxem/macOS je možné je dekomprimovat snadno pomocí `gzip -d <název souboru>`. Tento příkaz soubor dekomprimuje a zpřístupní na stejném názvu souboru, pouze bez přípony `.gz`.

## Instalace

Projekt používá Pipenv. Po nainstalování nástroje `pipenv` můžeme skript zprovoznit snadno pomocí:

1. `pipenv sync`
2. `pipenv shell`
3. Nyní můžeme pouštět skripty, například: `python sen_anal.py`
