import nltk
from nltk.stem import PorterStemmer, WordNetLemmatizer
from nltk.tokenize import word_tokenize

text = "The cats in this book are not your typical furry companions. They embark on dangerous adventures, battling fiercely with other creatures. They also love Rapunzel."

ps = PorterStemmer()
stemmed_words = [ps.stem(word) for word in word_tokenize(text)]

print(stemmed_words)


lemmatizer = WordNetLemmatizer()
lemmatized_words = [lemmatizer.lemmatize(word) for word in word_tokenize(text)]

print(lemmatized_words)


tokens = word_tokenize(text)
pos_tags = nltk.pos_tag(tokens)

print(pos_tags)


tokens = word_tokenize(text)
pos_tags = nltk.pos_tag(tokens)
named_entities = nltk.ne_chunk(pos_tags)

print(named_entities)
