import json

import matplotlib.pyplot as plt

with open('compound_scores.json', 'r') as f:
    compound_scores = json.load(f)

# compound_scores = list(filter(lambda x: x != 0, compound_scores))

plt.violinplot(compound_scores)
plt.ylabel("Compound Score")
plt.title("Violin Plot of Compound Scores")
plt.show()

plt.hist(compound_scores, bins=100)
plt.xlabel("Compound Score")
plt.ylabel("Frequency")
plt.title("Distribution of Compound Scores")
plt.show()
