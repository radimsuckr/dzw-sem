import matplotlib.pyplot as plt
import nltk
from nltk.sentiment import SentimentIntensityAnalyzer
from nltk.tokenize import word_tokenize

reviews = [
    "I absolutely love this product. It has changed my life!",
    "This is the worst thing I've ever bought. Completely useless.",
    "It's okay, I guess. Nothing to write home about.",
    "I have never seen a product worse than this. It's absolutely terrible.",
    "I am definitely going to recommend this as much as I can!",
    "This is fine but could be much better if the authors put a bit more care into the design.",
]


# Tokenize and POS tag the reviews
tokenized_reviews = [nltk.pos_tag(word_tokenize(review)) for review in reviews]

# Print the first review to see what our tokenized data looks like
print(tokenized_reviews[0])


sia = SentimentIntensityAnalyzer()

# Analyze the sentiment of each review
sentiments = [sia.polarity_scores(review) for review in reviews]

# Print the sentiment analysis results
for i, sentiment in enumerate(sentiments):
    print(f"Review: {reviews[i]}")
    print(f"Sentiment: {sentiment}\n")


# We'll just use the "compound" score for simplicity
compound_scores = [sentiment["compound"] for sentiment in sentiments]

plt.bar(range(len(reviews)), compound_scores)
plt.xlabel("Review")
plt.ylabel("Sentiment Score")
plt.title("Sentiment Analysis of Customer Reviews")
plt.xticks(range(len(reviews)))
plt.show()
