import json
from multiprocessing import Pool, freeze_support

import nltk
from nltk.stem import PorterStemmer, WordNetLemmatizer


def process_review(review):
    # ps = PorterStemmer()
    # stemmed_words = [ps.stem(word[0]) for word in review]

    # lemmatizer = WordNetLemmatizer()
    # lemmatized_words = [lemmatizer.lemmatize(word[0]) for word in review]

    pos_tags = nltk.pos_tag([word[0] for word in review])
    named_entities = nltk.ne_chunk(pos_tags, binary=True)

    reviews_with_named_entities = []
    for word in named_entities:
        if hasattr(word, 'label'):
            reviews_with_named_entities.append(word)

    return {
        # 'stemmed': stemmed_words,
        # 'lemmatized': lemmatized_words,
        'ner': named_entities,
        'ner_list': reviews_with_named_entities,
    }


if __name__ == '__main__':
    freeze_support()

    with open('tokenized_reviews.json', 'r') as f:
        tokenized_reviews = json.load(f)
        tokenized_reviews = tuple(review for ix, review in enumerate(tokenized_reviews) if ix % 1000 == 0)

    with Pool() as pool:
        result = pool.map(process_review, tokenized_reviews)

    # result = tuple(review for review in result if review['ner_list'])

    with open('ner_result.json', 'w+') as f:
        json.dump(result, f)
