import json

if __name__ == '__main__':
    with open('final_results.json', 'r') as f:
        ner_result = json.load(f)

    reviews_without_ne = 0
    most_common_word = {}
    for item in ner_result:
        if not item['ner_list']['nes_list']:
            reviews_without_ne += 1
            continue

        for word in item['ner_list']['nes_list']:
            if word[0][0] not in most_common_word:
                most_common_word[word[0][0]] = 1
            else:
                most_common_word[word[0][0]] += 1

    print(f'MCW: {len(most_common_word)}')
    most_common_word = {k: v for k, v in most_common_word.items() if v > 1}
    print(f'FMCV: {len(most_common_word)}')
    most_common_word_sorted = {k: v for k, v in
                               sorted(most_common_word.items(), key=lambda item: item[1], reverse=True)}
    print('---\n' + '\n'.join(f'{k}: {v}' for k, v in most_common_word_sorted.items()) + '\n---')

    def analyze_ne(ne):
        reviews_containing_ne = [item for item in ner_result if ne in
                                 [word[0][0].lower() for word in item['ner_list']['nes_list'] if item['ner_list']['nes_list']]]
        print(f'Reviews containing {ne}: {len(reviews_containing_ne)}')
        norelco_reviews_and_sentiments = [{'review': item['review'], 'sentiment': item['sentiment']}
                                          for item in ner_result if ne in [word[0][0].lower() for word in item['ner_list']['nes_list'] if item['ner_list']['nes_list']]]
        for ix, item in enumerate(norelco_reviews_and_sentiments):
            print(f'Sentiment: {item["sentiment"]["compound"]}\n{item["review"][:]}')
        return reviews_containing_ne

    results = {
        'norelco': analyze_ne('norelco'),
        'norelcos': analyze_ne('norelcos'),
        'perfect': analyze_ne('perfect'),
        'poor': analyze_ne('poor'),
        'not': analyze_ne('not'),
    }

    with open('table_data.json', 'w+') as f:
        json.dump(results, f, indent=4)

    print(f'Total: {len(ner_result)}\nReviews without NE: {reviews_without_ne}\nReviews with NE: {len(ner_result) - reviews_without_ne}')
